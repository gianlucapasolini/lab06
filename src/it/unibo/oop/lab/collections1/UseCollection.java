package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {
	
	private static final int ELEMS = 100000;
	private static final int TO_MS = 1_000_000;

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
        ArrayList<Integer> gino = new ArrayList<Integer>();
    	
    	for(int i = 1000; i < 2000; i ++) {
    		gino.add(i);
    	}
    	
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	LinkedList<Integer> gino2 = new LinkedList<Integer>();
    	gino2.addAll(gino);
    	
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	Integer OldLast = gino2.getFirst();
    	gino2.set(0, gino2.getLast());
    	gino2.set(gino2.size() - 1, OldLast);
    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for (Integer tmp: gino2) {
    		System.out.println(tmp.toString());
    	}
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	gino.clear();
    	
    	long time = System.nanoTime();
    	
        for (int i = 1; i <= ELEMS; i++) {
            gino.add(0, i);
        }

        time = System.nanoTime() - time;
        System.out.println("Time to add in head " + ELEMS + " in ArrayList: " + time / TO_MS + "ms");
        
        gino2.clear();
    	
    	time = System.nanoTime();
    	
        for (int i = 1; i <= ELEMS; i++) {
            gino2.addFirst(i);
        }

        time = System.nanoTime() - time;
        System.out.println("Time to add in head " + ELEMS + " in LinkedList: " + time / TO_MS + "ms");
        
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	
        time = System.nanoTime();
    	
        for (int i = 0; i < 1000; i++) {
            gino.get(50000);
        }

        time = System.nanoTime() - time;
        System.out.println("Time to check the 50000 element in ArrayList: " + time / TO_MS + "ms");
    	
        time = System.nanoTime();
    	
        for (int i = 0; i < 1000; i++) {
            gino2.get(50000);
        }

        time = System.nanoTime() - time;
        System.out.println("Time to check the 50000 element in LinkedList: " + time / TO_MS + "ms");
        
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        HashMap<String, Long> continentalPopulation = new HashMap<String, Long>();
        continentalPopulation.put("Africa" , (long) 1110635000);
        continentalPopulation.put("Americas" , (long) 972005000);
        continentalPopulation.put("Antarctica" , (long) 0);
        continentalPopulation.put("Asia" , 4298723000l);
        continentalPopulation.put("Europe" , (long) 742452000);
        continentalPopulation.put("Oceania" , (long) 38304000);
        
        /*
         * 8) Compute the population of the world
         */
        long tot = 0;
        for (long pop : continentalPopulation.values()) {
        	tot += pop;
        }
        
        System.out.println(tot);
        	
    }
}
